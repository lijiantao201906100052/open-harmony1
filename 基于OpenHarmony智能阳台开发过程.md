# 基于OpenHarmony的智能阳台

### 一、项目描述：

1. 能够实时监测是否下雨

2. 能够对接华为IoT平台，实时查看到当前状态（窗户开关情况、是否下雨）

3. 超过设定的阈值时本地启动关窗马达，并上报关窗信息

4. 本设备属于环境监测设备，将会集成到智能家居场景当中



### 二、系统架构图：



![基于Open Harmony智能阳台的系统架构图](https://gitee.com/lijiantao201906100052/open-harmony/raw/master/img2/%E5%9F%BA%E4%BA%8EOpen%20Harmony%E6%99%BA%E8%83%BD%E9%98%B3%E5%8F%B0%E7%9A%84%E7%B3%BB%E7%BB%9F%E6%9E%B6%E6%9E%84%E5%9B%BE.jpg)



### 三、系统流程图:



![基于Open Harmony智能阳台的系统流程图](https://gitee.com/lijiantao201906100052/open-harmony/raw/master/img2/%E5%9F%BA%E4%BA%8EOpen%20Harmony%E6%99%BA%E8%83%BD%E9%98%B3%E5%8F%B0%E7%9A%84%E7%B3%BB%E7%BB%9F%E6%B5%81%E7%A8%8B%E5%9B%BE.jpg)



### 四、系统原理图:



![基于Open Harmony智能阳台的原理图](https://gitee.com/lijiantao201906100052/open-harmony/raw/master/img2/%E5%9F%BA%E4%BA%8EOpen%20Harmony%E6%99%BA%E8%83%BD%E9%98%B3%E5%8F%B0%E7%9A%84%E5%8E%9F%E7%90%86%E5%9B%BE.png)



### 五、硬件分析：

#### 1.OpenHarmony开发板（Talkweb Niobe）拓维信息

​		拓维Niobe开发板是一块专门基于OpenHarmony 3.0LTS版本设计的开发板，板载高度集成了2.4GHz WLAN SoC芯片Hi3861。

​		Hi3861V100芯片适应于智能家电等物联网智能终端领域。

![OpenHarmony开发板](https://gitee.com/lijiantao201906100052/open-harmony/raw/master/img2/OpenHarmony.png)



#### 2.雨滴传感器模块

​	本项目选用雨滴传感器，目的是为了监测天气状况并转成AO电压输出。

（1）雨滴传感器采用FR-04双面材料，并用镀镍处理表面，具有抗氧化，导电性等性能。

（2）输出形式：模拟量AO电压输出。

（3）AO模拟输出，连接单片机的AD口检测滴在上面的雨量大小。（雨量越大输出电压越小）

![雨滴传感器](https://gitee.com/lijiantao201906100052/open-harmony/raw/master/img2/%E9%9B%A8%E6%BB%B4%E4%BC%A0%E6%84%9F%E5%99%A8.png)



#### 3.L298N直流电机驱动模块

​		本项目选用L298N直流电机驱动模块，目的是驱动直流电机的运作。

（1）本模块使用L298N作为主驱动芯片，具有驱动能力强，发热量低，抗干扰能力强的特点。

（2）L298N直流电机驱动模块，支持双轴控制，其电路原理就是两个H桥电路。

（3）该模块可以直接驱动两路3-30V直流电机，并提供5V输出接口，可以方便的控制直流电机速度和方向

（4）该模块和控制端口的接口：控制直流电机时IN1、IN2和ENA为一组，IN1和IN2是逻辑信号，控制电机的正反转；OUT1和OUT2它们控制电机A，接在A+和A-。

![L298N](https://gitee.com/lijiantao201906100052/open-harmony/raw/master/img2/L298N.png)

#### 4.直流电机

​		本项目选用直流电机目的是为了模拟窗户开关的情景。

​		电机正转一定角度表示开窗，反转一定角度表示关窗。

![直流电机](https://gitee.com/lijiantao201906100052/open-harmony/raw/master/img2/%E7%9B%B4%E6%B5%81%E7%94%B5%E6%9C%BA.png)

​		注：直流电机的工作电压为5V左右。



### 六、软件分析

#### 1.环境配置

​	基于OpenHarmony开发板的环境搭建及烧写过程（拓维Niobe开发板）文章已发布于Gitee上。

​	网站地址：https://gitee.com/lijiantao201906100052/open-harmony/blob/master/OpenHarmony%E5%BC%80%E5%8F%91%E6%9D%BF%E7%9A%84%E7%8E%AF%E5%A2%83%E6%90%AD%E5%BB%BA.md

#### 2.代码分析

##### （1）电机配置部分

​	使用GPIO7、GPIO8管脚控制电机（正反转、速度等）

​	使用两个管脚控制电机正反转，使用PWM输出电压控制电机转动速度。

###### ①管脚初始化

```c
	IoTGpioInit//GPIO初始化

	IoTIoSetFunc//GPIO使能

	IoTGpioSetDir//设置GPIO方向---IOT_GPIO_DIR_OUT---输出

	IoTGpioSetOutputVal//设置GPIO输出电平

	IoTPwmInit//初始化PWM设备
```



###### ②设置马达的开关状态

​	开窗情景由MotorStatusSet(ON);电机正转。

​	关窗情景由MotorStatusSet(OFF);电机反转。

​	启动PWM信号输出：使用PWM给定输出频率和相应的占空比指定给端口。

```c
IoTPwmStart(IOT_PWM_PORT_PWM1, 30, 15000);//开始PWM---正转---开窗

usleep(400000);//延时400ms

IoTPwmStop(IOT_PWM_PORT_PWM1);//停止PWM 
```



##### （2）雨滴传感器部分

​	使用GPIO4管脚控制雨滴传感器（有雨输出低电平，雨量越大电压越低）

###### ①管脚初始化

```c
	IoTGpioInit//GPIO初始化

	IoTIoSetFunc//GPIO使能

	IoTGpioSetDir//设置GPIO方向---IOT_GPIO_DIR_IN---输入
```

###### 	

###### ②使用ADC采集

```
ret = IoTAdcRead(IOT_ADC_CHANNEL_1, &val, IOT_ADC_EQU_MODEL_8, IOT_ADC_CUR_BAIS_DEFAULT, 256);//ADC转换通道号为ADC1
```

​	最终ADC值转换成对应的电压值。



##### （3）连接华为IoT平台部分

​	设备端与云端通讯的主要协议为MQTT协议。

​	需先在华为云IoT平台上创建产品并且注册对应的设备，华为IoT平台就会分配给设备一个设备ID，设备端可通过设备ID和相应的密钥来确保安全可信的连接到华为IoT平台。

​	连接华为IoT平台后，设备端可向华为IoT平台上传设备状态、数据等信息。同时华为IoT平台也可以下发命令给设备端，设备端收到命令后进行响应。

###### ①Wi-Fi连接配置

​	引用源码上的wifi_connect.c函数编写代码，连接Wi-Fi需要用到本地的Wi-Fi账号密码。

###### ②华为IoT平台的搭建

​	在华为云平台---设备接入IoT---创建一个产品，并添加服务、属性、命令等内容。

​	服务信息：（服务ID、服务类型）
​	属性信息：（属性名称、数据类型）
​	命令信息：（命令名称、参数名称、数据类型、长度、枚举）

![华为IoT平台配置](https://gitee.com/lijiantao201906100052/open-harmony/raw/master/img2/6.png)

​	新增测试设备--填写设备名称、设备标识码--创建成功则会得到两条信息：设备ID、设备密钥	

​	注：代码中的服务ID、属性名称、命令名称等与华为IoT平台一一对应

​	设备对接信息网站填写设备ID、设备密钥：https://iot-tool.obs-website.cn-north-4.myhuaweicloud.com/

​	生成相应信息：Clienid、Username、Password。（每个人生成的信息都是不一样的）

```
	#define CLIENT_ID "62062355de9933029be769ff_0_0_2022021109"
	#define USERNAME "62062355de9933029be769ff"
	#define PASSWORD "2d524c6fdd13a6c2392e61e16bac91053272c5cc3dcad72d7b87"
```

​	引用源码上的oc_mqtt.c和oc_mqtt_profile_package.c函数编写代码。

###### ③上传数据的配置

```
	oc_mqtt_profile_service_t service;//定义Service级别数据，该处对应云平台配置的Balcony服务下的数据
    oc_mqtt_profile_kv_t voltage;//定义属性级别数据，该处对应云平台配置的属性voltage信息
    oc_mqtt_profile_kv_t Window_flag;//定义属性级别数据，该处对应云平台配置的属性Window_flag信息
    oc_mqtt_profile_kv_t Rain_flag;//定义属性级别数据，该处对应云平台配置的属性Rain_flag信息
    oc_mqtt_profile_kv_t motor;//定义属性级别数据，该处对应云平台配置的属性motor信息

    service.event_time = NULL;
    service.service_id = "Balcony";//对应云平台配置的服务ID
    service.service_property = &voltage;//在Balcony服务下添加属性信息
    service.nxt = NULL;//该产品上报数据中仅存在Balcony一种服务，所以next指针为空

    voltage.key = "voltage";//对应云平台配置的属性名称
    voltage.value = &report->voltage;//voltage的取值由设备实际状态voltage决定。
    voltage.type = EN_OC_MQTT_PROFILE_VALUE_FLOAT;//对应云平台配置的数据类型
    voltage.nxt = &Window_flag;//继续添加Balcony服务下的另一属性。

    Window_flag.key = "Window_flag";
    Window_flag.value = g_app_cb.Window_flag ? "ON" : "OFF";
    Window_flag.type = EN_OC_MQTT_PROFILE_VALUE_STRING;
    Window_flag.nxt = &Rain_flag;

    Rain_flag.key = "Rain_flag";
    Rain_flag.value = g_app_cb.Rain_flag ? "ON" : "OFF";
    Rain_flag.type = EN_OC_MQTT_PROFILE_VALUE_STRING;
    Rain_flag.nxt = &motor;

    motor.key = "Motor_Status";
    motor.value = g_app_cb.motor ? "ON" : "OFF";//Motor_Status的ON,OFF取值由设备实际
    motor.type = EN_OC_MQTT_PROFILE_VALUE_STRING;
    motor.nxt = NULL;//Balcony服务下没有其它属性了，next置为null。

    oc_mqtt_profile_propertyreport(USERNAME, &service);//打包数据
```

###### ④下发命令控制电机的配置

```
	if (0 == strcmp(cJSON_GetStringValue(obj_cmdname), "Balcony_Control_Motor"))
    {
        obj_paras = cJSON_GetObjectItem(obj_root, "Paras");
        if (NULL == obj_paras)
        {
            goto EXIT_OBJPARAS;
        }
        obj_para = cJSON_GetObjectItem(obj_paras, "Motor");
        if (NULL == obj_para)
        {
            goto EXIT_OBJPARA;
        }
        //操作电动马达
        if (0 == strcmp(cJSON_GetStringValue(obj_para), "ON"))
        {
            g_app_cb.motor = 1;
            MotorStatusSet(ON);//开窗--电机正转
            g_app_cb.Window_flag = 1;
            Window_flag = 1;
        }
        else
        {
            g_app_cb.motor = 0;
            MotorStatusSet(OFF);//关窗--电机反转
            g_app_cb.Window_flag = 0;
            Window_flag = 0;
        }
```

​	经过层层解析，最终解出对应的命令，并设置到设备中。



### 七、问题总结及解决过程

#### 问题一：环境搭建。

​	搭建好一个开发环境是非常有必要的，搭建好环境对后面开发板的学习及OpenHarmony的开发都有一个事半功倍的作用，从安装虚拟机，下载源码，代码编写以及编译调试等步骤，若其中一步出现错误则可能在往后的开发过程中会出现不能预知的错误。对此经过几天的查阅资料与观看一些导师的课程等，最终完成了环境的搭建以及烧录过程。对此也编写了一篇《基于OpenHarmony开发板的环境搭建及烧录过程（拓维Niobe开发板）》的文章。

#### 问题二：烧录问题。

​	在烧录的时候，如果外接的传感器（雨滴传感器）的VCC接口接在开发板的5V上可能会导致烧录的时候复位不成功。

#### 问题三：硬件连接。

​	在硬件连接的过程，电机驱动模块的GND没有和开发板的GND相连，导致电机不转，输出端电压为0V。查阅了资料发现L298N供电的12V如果是用另外电源供电的话，那么需要将单片机的GND和驱动模块的GND连接起来，这样单片机上过来的逻辑信号才有个参考点，板载12V稳压芯片的输入引脚和电机供电驱动接线端子导通的。

#### 问题四：连接华为云。

​	在连接华为IoT平台的过程中，我们需要注意的问题是代码中的服务ID、属性名称、命令名称等信息与华为IoT平台一一对应，否则会导致数据上传不成功或者命令下发不成功。



### 八、项目作品演示

1. 能在华为IoT平台上下发命令控制电机的转动。

2. 可在雨滴传感器上滴上水珠，达到预定的阈值时，电机转动模拟关窗。
3. 能够在华为IoT平台上查看相关传感器测得的数值以及窗户（是否开窗）、环境（是否下雨）状态的情况。



​	Wi-Fi连接成功串口显示：

![Wi-Fi连接成功串口显示](https://gitee.com/lijiantao201906100052/open-harmony/raw/master/img2/Wi-Fi%E8%BF%9E%E6%8E%A5%E6%88%90%E5%8A%9F%E4%B8%B2%E5%8F%A3%E6%98%BE%E7%A4%BA.png)



​	华为IoT平台在线调试---可看到数据接收和命令发送的情况：

![华为IoT平台在线调试](https://gitee.com/lijiantao201906100052/open-harmony/raw/master/img2/5.png)



​	华为IoT平台查看到最新的上报数据：

![华为IoT平台查看上报数据](https://gitee.com/lijiantao201906100052/open-harmony/raw/master/img2/55.png)



​	以下分别对四个情景进行分析与说明：

##### 	情景一：无雨天气并且使用华为IoT平台下发命令开窗。

​	当华为IoT平台下发命令Balcony_Control_Motor--->ON；则会执行`MotorStatusSet(ON);`语句窗户马达则会转动将窗户打开并且将`g_app_cb.Window_flag = 1;`和`g_app_cb.motor = 1;`等数据信息上传回华为IoT平台。（Window_flag = ON 表示窗户开着）

​	华为IoT平台数据信息同步更新：

![情景一下发命令](https://gitee.com/lijiantao201906100052/open-harmony/raw/master/img2/1.png)

![情景一查看数据](https://gitee.com/lijiantao201906100052/open-harmony/raw/master/img2/11.png)

​	硬件图如下：

![情景一](https://gitee.com/lijiantao201906100052/open-harmony/raw/master/img2/%E6%83%85%E6%99%AF%E4%B8%80.png)

​	串口调试助手：

![情景一串口调试助手](https://gitee.com/lijiantao201906100052/open-harmony/raw/master/img2/%E6%83%85%E6%99%AF%E4%B8%80%E4%B8%B2%E5%8F%A3%E8%B0%83%E8%AF%95%E5%8A%A9%E6%89%8B.png)



##### 情景二：无雨天气并且使用华为IoT平台下发命令关窗。

​	当华为IoT平台下发命令Balcony_Control_Motor--->OFF；则会执行`MotorStatusSet(OFF);`语句窗户马达则会转动将窗户关闭并且将`g_app_cb.Window_flag = 0;`和`g_app_cb.motor = 0;`等数据信息上传回华为IoT平台。（Window_flag = OFF 表示窗户关着）

​	华为IoT平台数据信息同步更新：

![情景二下发命令](https://gitee.com/lijiantao201906100052/open-harmony/raw/master/img2/2.png)

![情景二查看数据](https://gitee.com/lijiantao201906100052/open-harmony/raw/master/img2/22.png)

​	硬件图如下：

![情景二实物图](https://gitee.com/lijiantao201906100052/open-harmony/raw/master/img2/%E6%83%85%E6%99%AF%E4%BA%8C%E5%AE%9E%E7%89%A9%E5%9B%BE.png)

​	串口调试助手：

![情景二串口调试助手](https://gitee.com/lijiantao201906100052/open-harmony/raw/master/img2/%E6%83%85%E6%99%AF%E4%BA%8C%E4%B8%B2%E5%8F%A3%E8%B0%83%E8%AF%95%E5%8A%A9%E6%89%8B.png)



##### 情景三：窗户开着，检测到下雨天气，则会关窗并且上报数据。

​	当窗户开着的情况，天气从晴朗天气变成下雨天气，当雨滴检测板检测到雨滴，并且超过设定的阈值的时候，则会执行`MotorStatusSet(OFF);`语句，窗户马达将转动将窗户关闭并且将`g_app_cb.Rain_flag = 1;g_app_cb.Window_flag = 0;g_app_cb.motor = 0;`等数据信息上传回华为IoT平台。（Rain_flag = ON 表示下雨天气；Window_flag = OFF 表示窗户关着）

（天气情况：晴朗天气变下雨天气，窗户情况：开着窗户变关着窗户）

​	华为IoT平台数据信息同步更新：

![情景三上报数据](https://gitee.com/lijiantao201906100052/open-harmony/raw/master/img2/3.png)

![情景三查看数据](https://gitee.com/lijiantao201906100052/open-harmony/raw/master/img2/33.png)

​	硬件图如下：

![情景三实物图](https://gitee.com/lijiantao201906100052/open-harmony/raw/master/img2/%E6%83%85%E6%99%AF%E4%B8%89%E5%AE%9E%E7%89%A9%E5%9B%BE.png)

​	串口调试助手：

![情景三串口调试助手](https://gitee.com/lijiantao201906100052/open-harmony/raw/master/img2/%E6%83%85%E6%99%AF%E4%B8%89%E4%B8%B2%E5%8F%A3%E8%B0%83%E8%AF%95%E5%8A%A9%E6%89%8B.png)



##### 情景四：下完雨后，雨滴检测板没有雨滴。

​	当天气从下雨天气变成晴朗天气，雨滴检测板没有检测到雨滴（此处为了方便实验，使用了纸巾擦除雨滴检测板上的水），`g_app_cb.Rain_flag = 0;`等数据信息上传回华为IoT平台。（Rain_flag = OFF 表示无雨天气）

​	华为IoT平台数据信息同步更新：

![情景四上报数据](https://gitee.com/lijiantao201906100052/open-harmony/raw/master/img2/4.png)

![情景四查看数据](https://gitee.com/lijiantao201906100052/open-harmony/raw/master/img2/44.png)

​	硬件图如下：

![535aba86edbb40a74d69330c9993985](https://gitee.com/lijiantao201906100052/open-harmony/raw/master/img2/%E6%83%85%E6%99%AF%E5%9B%9B%E5%AE%9E%E7%89%A9%E5%9B%BE.png)

​	串口调试助手：

![情景四串口调试助手](https://gitee.com/lijiantao201906100052/open-harmony/raw/master/img2/%E6%83%85%E6%99%AF%E5%9B%9B%E4%B8%B2%E5%8F%A3%E8%B0%83%E8%AF%95%E5%8A%A9%E6%89%8B.png)



### 九、心得体会

​	在本次OpenHarmony成长计划活动中，在开发项目的过程中出现了大大小小的问题，例如环境搭建、硬件连接和连接华为云平台等问题，但最后我能够顺利的攻克下来，在实践过程中积累了不少经验，掌握了硬件连接的相关知识，这让我对OpenHarmony开发产生了更浓厚的兴趣，更想往下学习下去，学习一些更有深度和更有趣的OpenHarmony的知识。

