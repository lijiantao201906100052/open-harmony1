### 硬件说明：

#### 1.OpenHarmony开发板（Talkweb Niobe）拓维信息

​		拓维Niobe开发板是一块专门基于OpenHarmony 3.0LTS版本设计的开发板，板载高度集成了2.4GHz WLAN SoC芯片Hi3861。

​		Hi3861V100芯片适应于智能家电等物联网智能终端领域。

![OpenHarmony开发板](https://gitee.com/lijiantao201906100052/open-harmony/raw/master/img2/OpenHarmony.png)



#### 2.雨滴传感器模块

​	本项目选用雨滴传感器，目的是为了监测天气状况并转成AO电压输出。

（1）雨滴传感器采用FR-04双面材料，并用镀镍处理表面，具有抗氧化，导电性等性能。

（2）输出形式：模拟量AO电压输出。

（3）AO模拟输出，连接单片机的AD口检测滴在上面的雨量大小。（雨量越大输出电压越小）

![雨滴传感器](https://gitee.com/lijiantao201906100052/open-harmony/raw/master/img2/%E9%9B%A8%E6%BB%B4%E4%BC%A0%E6%84%9F%E5%99%A8.png)



#### 3.L298N直流电机驱动模块

​		本项目选用L298N直流电机驱动模块，目的是驱动直流电机的运作。

（1）本模块使用L298N作为主驱动芯片，具有驱动能力强，发热量低，抗干扰能力强的特点。

（2）L298N直流电机驱动模块，支持双轴控制，其电路原理就是两个H桥电路。

（3）该模块可以直接驱动两路3-30V直流电机，并提供5V输出接口，可以方便的控制直流电机速度和方向

（4）该模块和控制端口的接口：控制直流电机时IN1、IN2和ENA为一组，IN1和IN2是逻辑信号，控制电机的正反转；OUT1和OUT2它们控制电机A，接在A+和A-。

![L298N](https://gitee.com/lijiantao201906100052/open-harmony/raw/master/img2/L298N.png)

#### 4.直流电机

​		本项目选用直流电机目的是为了模拟窗户开关的情景。

​		电机正转一定角度表示开窗，反转一定角度表示关窗。

![直流电机](https://gitee.com/lijiantao201906100052/open-harmony/raw/master/img2/%E7%9B%B4%E6%B5%81%E7%94%B5%E6%9C%BA.png)

​		注：直流电机的工作电压为5V左右。