### 项目作品演示

1. 能在华为IoT平台上下发命令控制电机的转动。

2. 可在雨滴传感器上滴上水珠，达到预定的阈值时，电机转动模拟关窗。
3. 能够在华为IoT平台上查看相关传感器测得的数值以及窗户（是否开窗）、环境（是否下雨）状态的情况。



​	Wi-Fi连接成功串口显示：

![Wi-Fi连接成功串口显示](https://gitee.com/lijiantao201906100052/open-harmony/raw/master/img2/Wi-Fi%E8%BF%9E%E6%8E%A5%E6%88%90%E5%8A%9F%E4%B8%B2%E5%8F%A3%E6%98%BE%E7%A4%BA.png)



​	华为IoT平台在线调试---可看到数据接收和命令发送的情况：

![华为IoT平台在线调试](https://gitee.com/lijiantao201906100052/open-harmony/raw/master/img2/5.png)



​	华为IoT平台查看到最新的上报数据：

![华为IoT平台查看上报数据](https://gitee.com/lijiantao201906100052/open-harmony/raw/master/img2/55.png)



​	以下分别对四个情景进行分析与说明：

##### 	情景一：无雨天气并且使用华为IoT平台下发命令开窗。

​	当华为IoT平台下发命令Balcony_Control_Motor--->ON；则会执行`MotorStatusSet(ON);`语句窗户马达则会转动将窗户打开并且将`g_app_cb.Window_flag = 1;`和`g_app_cb.motor = 1;`等数据信息上传回华为IoT平台。（Window_flag = ON 表示窗户开着）

​	华为IoT平台数据信息同步更新：

![情景一下发命令](https://gitee.com/lijiantao201906100052/open-harmony/raw/master/img2/1.png)

![情景一查看数据](https://gitee.com/lijiantao201906100052/open-harmony/raw/master/img2/11.png)

​	硬件图如下：

![情景一](https://gitee.com/lijiantao201906100052/open-harmony/raw/master/img2/%E6%83%85%E6%99%AF%E4%B8%80.png)

​	串口调试助手：

![情景一串口调试助手](https://gitee.com/lijiantao201906100052/open-harmony/raw/master/img2/%E6%83%85%E6%99%AF%E4%B8%80%E4%B8%B2%E5%8F%A3%E8%B0%83%E8%AF%95%E5%8A%A9%E6%89%8B.png)



##### 情景二：无雨天气并且使用华为IoT平台下发命令关窗。

​	当华为IoT平台下发命令Balcony_Control_Motor--->OFF；则会执行`MotorStatusSet(OFF);`语句窗户马达则会转动将窗户关闭并且将`g_app_cb.Window_flag = 0;`和`g_app_cb.motor = 0;`等数据信息上传回华为IoT平台。（Window_flag = OFF 表示窗户关着）

​	华为IoT平台数据信息同步更新：

![情景二下发命令](https://gitee.com/lijiantao201906100052/open-harmony/raw/master/img2/2.png)

![情景二查看数据](https://gitee.com/lijiantao201906100052/open-harmony/raw/master/img2/22.png)

​	硬件图如下：

![情景二实物图](https://gitee.com/lijiantao201906100052/open-harmony/raw/master/img2/%E6%83%85%E6%99%AF%E4%BA%8C%E5%AE%9E%E7%89%A9%E5%9B%BE.png)

​	串口调试助手：

![情景二串口调试助手](https://gitee.com/lijiantao201906100052/open-harmony/raw/master/img2/%E6%83%85%E6%99%AF%E4%BA%8C%E4%B8%B2%E5%8F%A3%E8%B0%83%E8%AF%95%E5%8A%A9%E6%89%8B.png)



##### 情景三：窗户开着，检测到下雨天气，则会关窗并且上报数据。

​	当窗户开着的情况，天气从晴朗天气变成下雨天气，当雨滴检测板检测到雨滴，并且超过设定的阈值的时候，则会执行`MotorStatusSet(OFF);`语句，窗户马达将转动将窗户关闭并且将`g_app_cb.Rain_flag = 1;g_app_cb.Window_flag = 0;g_app_cb.motor = 0;`等数据信息上传回华为IoT平台。（Rain_flag = ON 表示下雨天气；Window_flag = OFF 表示窗户关着）

（天气情况：晴朗天气变下雨天气，窗户情况：开着窗户变关着窗户）

​	华为IoT平台数据信息同步更新：

![情景三上报数据](https://gitee.com/lijiantao201906100052/open-harmony/raw/master/img2/3.png)

![情景三查看数据](https://gitee.com/lijiantao201906100052/open-harmony/raw/master/img2/33.png)

​	硬件图如下：

![情景三实物图](https://gitee.com/lijiantao201906100052/open-harmony/raw/master/img2/%E6%83%85%E6%99%AF%E4%B8%89%E5%AE%9E%E7%89%A9%E5%9B%BE.png)

​	串口调试助手：

![情景三串口调试助手](https://gitee.com/lijiantao201906100052/open-harmony/raw/master/img2/%E6%83%85%E6%99%AF%E4%B8%89%E4%B8%B2%E5%8F%A3%E8%B0%83%E8%AF%95%E5%8A%A9%E6%89%8B.png)



##### 情景四：下完雨后，雨滴检测板没有雨滴。

​	当天气从下雨天气变成晴朗天气，雨滴检测板没有检测到雨滴（此处为了方便实验，使用了纸巾擦除雨滴检测板上的水），`g_app_cb.Rain_flag = 0;`等数据信息上传回华为IoT平台。（Rain_flag = OFF 表示无雨天气）

​	华为IoT平台数据信息同步更新：

![情景四上报数据](https://gitee.com/lijiantao201906100052/open-harmony/raw/master/img2/4.png)

![情景四查看数据](https://gitee.com/lijiantao201906100052/open-harmony/raw/master/img2/44.png)

​	硬件图如下：

![535aba86edbb40a74d69330c9993985](https://gitee.com/lijiantao201906100052/open-harmony/raw/master/img2/%E6%83%85%E6%99%AF%E5%9B%9B%E5%AE%9E%E7%89%A9%E5%9B%BE.png)

​	串口调试助手：

![情景四串口调试助手](https://gitee.com/lijiantao201906100052/open-harmony/raw/master/img2/%E6%83%85%E6%99%AF%E5%9B%9B%E4%B8%B2%E5%8F%A3%E8%B0%83%E8%AF%95%E5%8A%A9%E6%89%8B.png)