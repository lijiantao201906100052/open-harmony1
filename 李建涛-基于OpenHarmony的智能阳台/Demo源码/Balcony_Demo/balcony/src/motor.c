#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <math.h>

#include "cmsis_os2.h"
#include "motor.h"
#include "iot_errno.h"
#include "iot_gpio.h"
#include "iot_pwm.h"
#include "iot_adc.h"

static void MotorIoInit(void)
{
    //IN1--GPIO8--PWM1
    IoTGpioInit(IOT_GPIO_IO_GPIO_8);
    IoTIoSetFunc(IOT_GPIO_IO_GPIO_8, IOT_IO_FUNC_GPIO_8_PWM1_OUT);
    IoTGpioSetDir(IOT_GPIO_IO_GPIO_8, IOT_GPIO_DIR_OUT);
    IoTGpioSetOutputVal(IOT_GPIO_IO_GPIO_8, IOT_GPIO_VALUE0);//低电平
    IoTPwmInit(IOT_PWM_PORT_PWM1);

    //IN2--GPIO7--PWM0
    IoTGpioInit(IOT_GPIO_IO_GPIO_7);
    IoTIoSetFunc(IOT_GPIO_IO_GPIO_7, IOT_IO_FUNC_GPIO_7_PWM0_OUT);
    IoTGpioSetDir(IOT_GPIO_IO_GPIO_7, IOT_GPIO_DIR_OUT);
    IoTGpioSetOutputVal(IOT_GPIO_IO_GPIO_7, IOT_GPIO_VALUE0);//低电平
    IoTPwmInit(IOT_PWM_PORT_PWM0);

}

void YUDI_Init(void)
{
    //雨滴传感器---模拟信号---检测水量大小---ADC初始化
    IoTGpioInit(IOT_GPIO_IO_GPIO_4);
    IoTIoSetFunc(IOT_GPIO_IO_GPIO_4, IOT_IO_FUNC_GPIO_4_GPIO);
    IoTGpioSetDir(IOT_GPIO_IO_GPIO_4, IOT_GPIO_DIR_IN);
}

/**
 * @brief 初始化马达开发板的硬件状态
 */ 
void Motor_Init(void)
{
    MotorIoInit();
}

/*
void LightStatusSet(Motor_Status_ENUM status)
{
    if (status == ON)
        IoTGpioSetOutputVal(IOT_GPIO_IO_GPIO_9, IOT_GPIO_VALUE0);//核心板上的LEd灯--低电平--亮
        usleep(400000);
    if (status == OFF)
        IoTGpioSetOutputVal(IOT_GPIO_IO_GPIO_9, IOT_GPIO_VALUE1);//核心板上的LEd灯--高电平--灭
        usleep(400000);
}*/

/**
 * @brief 设置马达的开关状态
 */
void MotorStatusSet(Motor_Status_ENUM status)
{
    if (status == ON)
        //IoTGpioSetOutputVal(IOT_GPIO_IO_GPIO_8, 1); //设置GPIO_8输出高电平打开电机
        IoTPwmStart(IOT_PWM_PORT_PWM1, 30, 15000);//正转--开窗
        usleep(400000);
        IoTPwmStop(IOT_PWM_PORT_PWM1);
    if (status == OFF)
        //IoTGpioSetOutputVal(IOT_GPIO_IO_GPIO_8, 0); //设置GPIO_8输出低电平关闭电机
        IoTPwmStart(IOT_PWM_PORT_PWM0, 30, 15000);//反转--关窗
        usleep(400000);
        IoTPwmStop(IOT_PWM_PORT_PWM0);
}
