# 基于OpenHarmony开发板的环境搭建及烧录过程（拓维Niobe开发板）



## 前言

​				此次基于OpenHarmony开发板的环境搭建及烧录过程（拓维Niobe开发板）是在参加OpenHarmony开源开发者成长计划的活动中进行学习和探索的一个过程。

​				万事开头难，要完成一个项目，首先第的一件事就是环境的搭建，搭建好环境对后面开发板的学习及OpenHarmony的开发都有一个事半功倍的作用，从安装虚拟机，下载源码，代码编写以及编译调试等步骤，若其中一步出现错误则可能在往后的开发过程中会出现不能预知的错误，所以这是一个最为艰难但也是最为重要的过程。对于小白来说是非常困难的一件事，从配置环境开始到编译成功可能会花至少一两天，没错我就是那个小白，从0开始学OpenHarmony开发板。

​				由于环境搭建所涉及的领域及组件会比较多，所以我在搭建环境的时候也遇到了不少的问题，也走了不少弯路一遍一遍的错误以及一次又一次从头开始，让我更有决心不达目的不罢休。通过不断的查找资料，观看一些导师的课程，以及不断的尝试和探索，最终完成了环境的搭建以及烧录过程。我觉得我有必要帮助一些像我一样想学习OpenHarmony开发板的同学，所以总结了一些环境搭建的过程，接下来就是环境搭建的操作过程。



## 一、准备工作（下载工具）

- 下载并安装虚拟机VMware Workstation 。

​		VMware Workstation Player（免费版）下载地址：https://www.vmware.com/products/workstation-player/workstation-player-evaluation.html

​		VMware Workstation Pro（专业版）下载地址：https://www.vmware.com/cn/products/workstation-pro.html

- 下载Ubuntu20.04镜像。

​		下载地址：https://releases.ubuntu.com/focal/ubuntu-20.04.4-desktop-amd64.iso

- 下载并安装MobaXterm工具。

​		下载地址：https://en.softonic.com/download/moba/windows/post-download

- 下载并安装RaiDrive工具。

​		下载地址：https://forspeed.rbread05.cn/down/newdown/5/28/RaiDrive.rar

- 下载并安装开发板USB驱动。（CH341SER.EXE-CH340/CH341-USB转串口Windows驱动程序）

​		下载地址：http://www.wch.cn/search?q=ch340g&t=downloads

- 下载并安装编译器VS Code。

​		下载地址：https://code.visualstudio.com/

- 下载烧录工具HiBurn。

​		下载地址：https://gitee.com/talkweb_oh/niobe/blob/master/applications/docs/tools/HiBurn.exe





## 二、部署Linux环境

- 安装Ubuntu（在VMware中安装下载好的Ubuntu20.04）。
- 安装完虚拟机后获取IP地址。

​	1.新建虚拟机

​	打开VMware Workstation，点击创建新的虚拟机

![image-20220114022455715](https://openharmony-1310563882.cos.ap-nanjing.myqcloud.com/img1/image-20220114022455715.png)



​	2.设置镜像文件

​	创建新的虚拟机后需要选择IOS映像文件，在虚拟机设置 -> CD/DVD（SATA），然后选择我们下载好的Ubuntu20.04镜像文件的路径。

![image-20220114023121588](https://openharmony-1310563882.cos.ap-nanjing.myqcloud.com/img1/image-20220114023121588.png)



​	3.设置网络适配器

​	在虚拟机设置 -> 网络适配器，网络连接选择NAT模式即可。

![image-20220114023139360](https://openharmony-1310563882.cos.ap-nanjing.myqcloud.com/img1/image-20220114023139360.png)



​	4.开启虚拟机，第一次需要安装。

​	安装完后打开终端获取ip地址。输入语句：`ifconfig`

​	获取IP地址后并记录下来，每个人的IP地址都不一样。

​	如果获取不成功（虚拟机连不上网络）尝试更换一下网络路径方式：虚拟机 -> 设置 -> 网络适配器 -> 网络路径

![image-20220114024319462](https://openharmony-1310563882.cos.ap-nanjing.myqcloud.com/img1/image-20220114024319462.png)

*如果查看IP地址的时候出现以下情况：

![image-20220115234649088](https://openharmony-1310563882.cos.ap-nanjing.myqcloud.com/img1/image-20220115234649088.png)

- 根据提示，使用 `sudo apt install net-tools` 命令，安装网络工具。
- 如果还不行也可以安装一下VMware tools工具。

## 三、使用远程链接工具MobaXterm链接服务器

1. 在MobaXterm工具里 Session -> SSH 输入信息，远程地址（获取到的IP地址）。

   ![image-20220114025552556](https://openharmony-1310563882.cos.ap-nanjing.myqcloud.com/img1/image-20220114025552556.png)

2. 输入相关信息登录。

   login as：输入虚拟机用户名

   Password：输入虚拟机密码

   成功登录后如下：

   ![image-20220114025658985](https://openharmony-1310563882.cos.ap-nanjing.myqcloud.com/img1/image-20220114025658985.png)



## 四、使用RaiDrive工具将Linux文件远程映射到Windows上

1.将RaiDrive工具切换为中文语言。

![image-20220114030647912](https://openharmony-1310563882.cos.ap-nanjing.myqcloud.com/img1/image-20220114030647912.png)



2.点击添加

- 选择NAS -> SFTP

- 取消勾选只读

- stfp：// （此处填写：获取到的IP地址）

- 输入虚拟机的账号和密码

- 确定

![image-20220114031007335](https://openharmony-1310563882.cos.ap-nanjing.myqcloud.com/img1/image-20220114031007335.png)



3.打开

![image-20220114031248475](https://openharmony-1310563882.cos.ap-nanjing.myqcloud.com/img1/image-20220114031248475.png)



4.在我的电脑里面的网络位置可以看到配置好的SFTP

![image-20220114031359571](https://openharmony-1310563882.cos.ap-nanjing.myqcloud.com/img1/image-20220114031359571.png)



## 五、安装samba服务共享文件夹

1.在/home/niobe/目录下新建一个文件夹命名为niobecode。

2.配置niobecode文件夹的读写权限。

```
sudo chmod 777 /home/niobe/niobe_code
```

3.将新建的niobecode文件夹配置到/etc/samba/smb.conf中。

- 使用spt-get安装

  ```
  sudo apt-get install samba
  ```

- 查看sanba版本，以及是否安装成功

  ```
  samba
  ```

- 进入smb.conf文件配置

  ```
  sudo vi /etc/samba/smb.conf
  ```

- 在smb.conf文件最后加上以下语句

  ```
  [niobecode] 
  comment = samba home directory 
  path = /home/niobe/
  public = yes 
  browseable = yes 
  public = yes 
  writeable = yes 
  read only = no
  valid users = niobe 
  create mask = 0777
  directory mask = 0777 
  #force user = nobody
  #force group = nogroup
  available = yes 
  ```

- 按下ESC后保存并退出

  ```
  ：wq
  ```

- 设samba密码

  ```
  sudo smbpasswd -a niobe
  ```

- 重新启动samba

  ```
  sudo service smbd restart
  ```

4.右键我的电脑 -> 映射网络驱动器 -> 输入IP地址，把共享文件夹映射到本地。

4.在我的电脑里面的网络位置可以看到niobecode文件夹。

![image-20220114032622527](https://openharmony-1310563882.cos.ap-nanjing.myqcloud.com/img1/image-20220114032622527.png)



## 六、获取源码

首先注册好Gitee账号。

进入共享文件夹niobecode

```
cd /home/niobe/niobecode
```

先安装git

```
sudo apt install git
```

使用 git https的下载方式

```
git clone https://gitee.com/talkweb_oh/niobe.git
```



## 七、配置docker编译环境

1.添加docker的官方GPC密钥。

```
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
```

2.添加仓库。

```
echo "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
```

3.安装docker

```
sudo apt-get update
```

```
sudo apt-get install docker-ce docker-ce-cli containerd.io
```

4.运行测试docker

```
sudo docker run hello-world
```

运行成功结果：

```
Hello from Docker!
This message shows that your installation appears to be working correctly.

To generate this message, Docker took the following steps:
 1. The Docker client contacted the Docker daemon.
 2. The Docker daemon pulled the "hello-world" image from the Docker Hub.
    (amd64)
 3. The Docker daemon created a new container from that image which runs the
    executable that produces the output you are currently reading.
 4. The Docker daemon streamed that output to the Docker client, which sent it
    to your terminal.

To try something more ambitious, you can run an Ubuntu container with:
 $ docker run -it ubuntu bash

Share images, automate workflows, and more with a free Docker ID:
 https://hub.docker.com/

For more examples and ideas, visit:
 https://docs.docker.com/get-started/
```

5.获取docker镜像

```
docker pull swr.cn-south-1.myhuaweicloud.com/openharmony-docker/openharmony-docker:0.0.5
```

*这里注意一点就是：

如果安装完docker后，执行docker相关的命令，出现以下报错：

原因分析：可能就是docker 进程使用 Unix Socket 而不是 TCP 端口。

在默认情况下，Unix socket 属于 root 用户，需要 root 权限才能访问。

解决方法：

- 方法1：使用 sudo 获取管理员权限，运行 docker 命令时在前面加上 sudo。

- 方法2：docker 守护进程启动的时候，会默认赋予名字为 docker 的用户组读写 Unix socket 的权限，因此只要创建 docker 用户组，并将当前用户加入到 docker 用户组中，那么当前用户就有权限访问 Unix socket 了，进而也就可以执行 docker 相关命令了。

  具体操作命名如下：

  ```
  sudo groupadd docker     		# 添加docker用户组
  sudo gpasswd -a $USER docker    # 将登陆用户加入到docker用户组中
  newgrp docker     				# 更新用户组
  docker images    				# 测试docker命令在不加sudo时是否可以使用
  ```

  

## 八、查看代码

在windows下，打开共享文件夹niobecode，将niobe文件夹直接拖拽到 VS code上即可打开代码。

![image-20220114035519417](https://openharmony-1310563882.cos.ap-nanjing.myqcloud.com/img1/image-20220114035519417.png)

## 九、编译代码

进入docker环境来编译源码

Niobe使用的是小型系统的docker编译环境:docker-openharmony:0.0.5

进入niobecode文件夹（OpenHarmony代码的根目录下）执行命令进入Docker构建环境

```
cd /home/niobe/niobecode
```

```
docker run -it -v $(pwd):/home/openharmony swr.cn-south-1.myhuaweicloud.com/openharmony-docker/openharmony-docker:0.0.5
```

*此时已经在Docker编译环境下

![image-20220114040145465](https://openharmony-1310563882.cos.ap-nanjing.myqcloud.com/img1/image-20220114040145465.png)

- 编译hb文件

1.输入命令：

```
cd niobe
```

2.输入命令：

```
hb set 
```

*设置工作目录。

3.输入 .   

*输入源码所在目录，点(.)表示当前目录。

4.通过回车确定选择，选择talkweb niobe_wifi_iot开发板。

![image-20220114040220260](https://openharmony-1310563882.cos.ap-nanjing.myqcloud.com/img1/image-20220114040220260.png)

5.输入命令：

```
hb build -b release -f
```

  执行编译。

![image-20220114040311218](https://openharmony-1310563882.cos.ap-nanjing.myqcloud.com/img1/image-20220114040311218.png)

*出现 build success 说明编译成功。



## 十、连接开发板并查看串口号

- 安装CH340驱动。

- 使用TypeC数据线，将拓维niobe开发板与电脑连接起来。

- 连接电脑时需要注意将虚拟机捕获USB的功能关闭。（会导致Windows查看不到串口）

- 查看串口号

  我的电脑-->右键-->管理-->设备管理器-->端口（COM和LPT）-->USB-SERIAL CH340（COMx）

  

## 十一、烧录程序

在Windows下打开Hiburn工具

- 点击Refresh就会出现串口号

![image-20220114040940407](https://openharmony-1310563882.cos.ap-nanjing.myqcloud.com/img1/image-20220114040940407.png)

- 点击Setting，选择 Com settings

- 在Com settings中设置Baud为：921600，点击确定 

![image-20220114041107249](https://openharmony-1310563882.cos.ap-nanjing.myqcloud.com/img1/image-20220114041107249.png)

- 点击Select file，在弹出的文件框中，选择路径，并选中：Hi3861_wifiiot_app_allinone.bin 文件

- 点击Auto burn复选框，然后点击Connect，此时Connect按钮变成Disconnect

- 按一下开发板上的复位按钮，开始烧录程序

![image-20220114042101432](https://openharmony-1310563882.cos.ap-nanjing.myqcloud.com/img1/image-20220114042101432.png)

- 出现Execution Successful字样，程序烧录完成。

![image-20220114041504393](https://openharmony-1310563882.cos.ap-nanjing.myqcloud.com/img1/image-20220114041504393.png)

- 烧录完后，点击Disconnect按钮断开连接，便于后面调测使用。

  

## 十二、查看串口打印日志

- 打开MobaXterm 点击：Session、Serial按钮。

- 设置Seral port为 Hiburn 同一个串口

- 设置Speed为 115200

- 点击OK

  ![image-20220114041856074](https://openharmony-1310563882.cos.ap-nanjing.myqcloud.com/img1/image-20220114041856074.png)

- 如果显示Unable to open...等字样，需要看一下Hiburn的链接状态是否为Disconnect。

- 复位开发板，此时COM打印出对应日志信息供查看。

  ![image-20220114041958115](https://openharmony-1310563882.cos.ap-nanjing.myqcloud.com/img1/image-20220114041958115.png)

​	SUCCESS！到此我们已经搭建好编译环境，并且成功的将代码烧录到开发板上并且能够正常的运行了！

## 自我介绍

​		我叫李建涛，就读于广州商学院大，我的专业是物联网工程，一名大三学生，也是参加了本次OpenHarmony开源开发者成长计划的活动，编写文章的目的是为了总结一些学习OpenHarmony开发板过程中的笔记及遇到问题的解决方法。